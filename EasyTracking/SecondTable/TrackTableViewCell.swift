//
//  SecondTableViewCell.swift
//  EasyTracking
//
//  Created by Willian Calazans on 25/03/18.
//  Copyright © 2018 alzan. All rights reserved.
//

import UIKit

class TrackTableViewCell: UITableViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var codLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func commonInit(imageName : String, title: String, local: String, data: String){
        mainImage.image = UIImage(named: imageName)
        
        titleLabel.text = title
        titleLabel.numberOfLines = 0
        
        codLabel.text = local

        statusLabel.text = data
        statusLabel.numberOfLines = 0

    }
    
}
