//
//  SecondViewController.swift
//  EasyTracking
//
//  Created by Willian Calazans on 24/03/18.
//  Copyright © 2018 alzan. All rights reserved.
//

import UIKit

class TrackViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mainTableView: UITableView!
    var stringPassed = String()
    
    var localData = [String]()
    var imageData = [String]()
    var nameArray = [String]()
    var dateData = [String]()
    
    @IBOutlet var emptyView: UIView!

    
     var refreshControl : UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        mainTableView.dataSource = self
        mainTableView.delegate = self
        
        let nipname = UINib(nibName: "TrackTableViewCell", bundle: nil)
        mainTableView.register(nipname, forCellReuseIdentifier: "showTrackingViewCell")
        
        mainTableView.tableFooterView = UIView()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        mainTableView.addSubview(refreshControl)

        do {
            getJSONFromUrl()
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func refresh(){
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        getJSONFromUrl()
        refreshControl.endRefreshing()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = mainTableView.dequeueReusableCell(withIdentifier: "showTrackingViewCell", for: indexPath) as! TrackTableViewCell
        
            cell.commonInit(imageName: imageData[indexPath.item], title: nameArray[indexPath.item], local: localData[indexPath.item], data: dateData[indexPath.item])
        
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vcName = "second"
//        let destination = self.storyboard?.instantiateViewController(withIdentifier: vcName) as! SecondViewController
//        destination.stringPassed = nameArray[indexPath.item]
//        self.navigationController?.pushViewController(destination, animated: true)
//        
//        print(nameArray[indexPath.item])
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func getJSONFromUrl(){
        
        let URL = "https://api.postmon.com.br/v1/rastreio/ect/" + stringPassed
        
        localData.removeAll()
        imageData.removeAll()
        nameArray.removeAll()
        dateData.removeAll()
        
        print(URL)
        
        
        let url = NSURL(string: URL)
        
        URLSession.shared.dataTask(with: (url as URL?)!, completionHandler: {(data, response, error) -> Void in
            
            if (data == nil){
                print("Sem conexão \(String(describing: data))")
                if (self.localData.count == 0){
                    print("EMPTY")
                    self.mainTableView.backgroundView = self.emptyView
                } else {
                    print("NOT EMPTY \(self.localData.count)")
                    self.mainTableView.backgroundView = nil
                }
            }
            else{
            if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary {
                
                print(jsonObj!.value(forKey: "historico")!)
                if let jsonArray = jsonObj!.value(forKey: "historico") as? NSArray{
                    for array in jsonArray.reversed(){
                        if let vetsDict = array as? NSDictionary {
                            _ = vetsDict.value(forKey: "situacao")
//                            self.nameArray.append((name as? String)!)
                            
                            let local = vetsDict.value(forKey: "detalhes")
                            let localLocal = vetsDict.value(forKey: "local")

                            
                            if local as? String == ""{
                                self.localData.append((localLocal as? String)!)
                                print(localLocal as! String)
                            } else{
                                self.localData.append((local as? String)!)
                            }
                                
                            let imageS = vetsDict.value (forKey: "situacao")
                            
                            print(imageS as! String)
                            
                            if imageS as? String == "Objeto encaminhado"{
                                print(imageS as! String)
                                self.imageData.append(("encaminhado"))
                                self.nameArray.append(("Objeto encaminhado"))
                            
                            } else if imageS as? String == "Objeto postado"{
                                print(imageS as! String)
                                self.imageData.append(("postado"))
                                self.nameArray.append(("Postado"))

                            } else if imageS as? String == "Objeto saiu para entrega ao destinatï¿½rio"{
                                self.imageData.append(("saiu"))
                                self.nameArray.append(("Saiu para entrega ao destinatário"))

                            } else if imageS as? String == "Objeto entregue ao destinatï¿½rio"{
                                self.imageData.append(("success"))
                                self.nameArray.append(("Entrega efetuada"))

                            } else if imageS as? String == "ï¿½rea com distribuiï¿½ï¿½o sujeita a prazo diferenciado" {
                                self.imageData.append(("time"))
                                self.nameArray.append(("Área com distribuição sujeita a prazo diferenciado"))
                            } else if imageS as? String == "Objeto recebido pelos Correios do Brasil"{
                                self.imageData.append(("brasil"))
                                self.nameArray.append(("Objeto recebido pelos Correios do Brasil"))
                            } else if imageS as? String == "Objeto ainda nï¿½o chegou ï¿½ unidade."{
                                self.imageData.append(("time"))
                                self.nameArray.append(("Objeto ainda não chegou à unidade"))
                            } else if imageS as? String == "Objeto aguardando retirada no endereï¿½o indicado"{
                                self.imageData.append(("time"))
                                self.nameArray.append(("Objeto aguardando retirada no endereço indicado"))
                            } else if imageS as? String == "Favor desconsiderar a informaï¿½ï¿½o anterior"{
                                self.imageData.append(("n_entregue"))
                                self.nameArray.append(("Favor desconsiderar a informação anterior"))
                            } else if imageS as? String == "Saï¿½da para entrega cancelada"{
                                self.imageData.append(("n_entregue"))
                                self.nameArray.append(("Saida para entrega cancelada"))
                            }
                            else if imageS as? String == "Objeto postado apï¿½s o horï¿½rio limite da unidade"{
                                self.imageData.append(("time"))
                                self.nameArray.append(("Objeto postado após o horário limite da unidade"))
                            }
                            else {
                                self.imageData.append(("null_reference"))
                                self.nameArray.append((imageS as? String)!)
                            }
                            
                            let dataJSON = vetsDict.value(forKey: "data")
                            self.dateData.append((dataJSON as? String)!)
                            
                            OperationQueue.main.addOperation({
                                self.mainTableView.reloadData()
                            })
                        }
                        
                        print(self.imageData.count)
                    }
                }
            }
            }
        }).resume()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.title = stringPassed
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let backItem = UIBarButtonItem()
//        backItem.title = "Cancelar"
//        navigationItem.backBarButtonItem = backItem
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    

}
