//
//  AddHelperViewController.swift
//  EasyTracking
//
//  Created by Willian Calazans on 12/04/18.
//  Copyright © 2018 alzan. All rights reserved.
//

import Foundation
import UIKit

class AddHelperViewController: UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        let vcName = "add"
        let destination = self.storyboard?.instantiateViewController(withIdentifier: vcName) as! AddTrackingViewController
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
}
