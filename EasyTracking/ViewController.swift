
//
//  ViewController.swift
//  EasyTracking
//
//  Created by Willian Calazans on 23/03/18.
//  Copyright © 2018 alzan. All rights reserved.
//

import UIKit
import SQLite3


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var mainTableView: UITableView!
    var trackingItems = [TrackingItems]()
    
    var insertCod = String()

    var db: OpaquePointer?
    
    var refreshControl : UIRefreshControl!

    @IBOutlet var emptyView: UIView!
    
    @IBAction func fab(_ sender: UIButton) {
    }
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
//        getJSONFromUrl()
        
        mainTableView.tableFooterView = UIView()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        mainTableView.addSubview(refreshControl)
        
        createDB()
        selectObject()
        
//        TrackingDB().insertObject(with: "999", ident: "999")
                
//        self.title = "UITableView"
        mainTableView.dataSource = self
        mainTableView.delegate = self
        
        let nipname = UINib(nibName: "TrackingTableViewCell", bundle: nil)
        mainTableView.register(nipname, forCellReuseIdentifier: "trackingViewCell")

        print("TABLE VIEW \(trackingItems.count)")
        
        DispatchQueue.global(qos: .background).async{
            print("DispatchQueue")
            DispatchQueue.main.async{
                print("DispatchQueue Main")
            }
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func createDB(){
        
        let fileUrl = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("AlzanDB.sqlite")
        
        if sqlite3_open(fileUrl.path, &db) == SQLITE_OK{
            print("OK DB")
            print("URL DB \(fileUrl)")
            createTable()
        }
    }
    
    @objc func refresh(){
        
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        var items : TrackingItems
        
        for i in 1..<trackingItems.count {
            items = trackingItems[i]
            getJSONFromUrl(cod: items.cod!)
        }
        selectObject()
        refreshControl.endRefreshing()
    }
    
    
   
    
    func createTable(){
        let createTable = "CREATE TABLE IF NOT EXISTS OBJETOS (COD TEXT PRIMARY KEY NOT NULL, ARCHIVE TEXT, STATUS TEXT, IDENTIFY TEXT);"
        
        if sqlite3_exec(db, createTable, nil, nil, nil) != SQLITE_OK{
            let errMsg = String(cString: sqlite3_errmsg(db))
            print("ERROR CREATE TABLE \(errMsg)")
        } else {
            print("That`s OK")
        }
    }
    
    
    func insertObject(with cod : String, ident : String){
        var stmt: OpaquePointer?
        
        createDB()

        let insertQuery = "INSERT INTO OBJETOS (COD, IDENTIFY, STATUS) VALUES ('\(cod)', '\(ident)', ' ')"

        if sqlite3_prepare(db, insertQuery, -1, &stmt, nil) != SQLITE_OK{
            let errMsg = String(cString: sqlite3_errmsg(db))
            print("ERROR PREPARE INSERT \(errMsg)")
        }
        
        print("INSERT INTO \(cod, ident)")
        print(cod)
        
        print("INSERT INTO \(sqlite3_step(stmt) )")
        
    }
    
    var statusOld = " "
    
    func selectObject(){
        trackingItems.removeAll()
        var stmt : OpaquePointer?
        
        let selectQuery = "SELECT COD, IDENTIFY, STATUS FROM OBJETOS"
        
        if sqlite3_prepare(db, selectQuery, -1, &stmt, nil) != SQLITE_OK {
            let errMsg = String(cString: sqlite3_errmsg(db))
            print("SELECT PREPARE INSERT \(errMsg)")
        }
        
        
        while (sqlite3_step(stmt) == SQLITE_ROW){
            let COD = sqlite3_column_text(stmt, 0)
            let IDENTIFY = sqlite3_column_text(stmt, 1)
            let STATUS = sqlite3_column_text(stmt, 2)
            
            let statusNull = ""

            let codNil = String(cString: COD!)
            
            statusOld = String(cString: STATUS!)
            
            print("NOT NULL \(statusNull)")
    
            getJSONFromUrl(cod: codNil)
            
            trackingItems.append(TrackingItems(cod: String(cString: COD!), identify: String(cString: IDENTIFY!), status: String(cString: STATUS!)))
            
    
           
            print("XXX COD \(String(cString: COD!))")
            print("XXX IDENTIFY \(String(cString: IDENTIFY!))")
            
            print("XXX COUNT \(trackingItems.count)")
        }
        
        
        if trackingItems.count == 0{
            print("EMPTY")
            mainTableView.backgroundView = emptyView
        } else {
            print("NOT EMPTY \(trackingItems.count)")
            mainTableView.backgroundView = nil
        }
        
        self.mainTableView.reloadData()
    }
    
    func deleteObject(with cod: String?){
        
        var stmt : OpaquePointer?
        createDB()
        
        let deleteQuery = "DELETE FROM OBJETOS WHERE COD = '\(cod!)'"
        
        if sqlite3_prepare(db, deleteQuery, -1, &stmt, nil) != SQLITE_OK{
            let error = sqlite3_errmsg(stmt)
            print("ERROR \(String(cString:error!))")
        } else {
            sqlite3_step(stmt)
            print("DELETE TRUE")
            selectObject()
        }
        
        if trackingItems.count == 0{
            print("EMPTY")
            mainTableView.backgroundView = emptyView
        } else {
            print("NOT EMPTY \(trackingItems.count)")
            mainTableView.backgroundView = nil
        }
        
        print(deleteQuery)
        
        sqlite3_finalize(stmt)
    }
    
    func updateObject(with cod: String?, status: String?){

        var stmt: OpaquePointer?
        createDB()
        
        let updateQuery = "UPDATE OBJETOS SET STATUS = '\(status!)' WHERE COD = '\(cod!)'"
        
        if sqlite3_prepare(db, updateQuery, -1, &stmt, nil) != SQLITE_OK{
            let error = sqlite3_errmsg(stmt)
            print("ERROR \(String(cString: error!))")
            
        } else {
            sqlite3_step(stmt)
            print("UPDATE TRUE \(updateQuery)")
//            selectObject()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
         print("CODIGO : \(insertCod)")

        if trackingItems.count == 0{
            print("EMPTY")
            mainTableView.backgroundView = emptyView
        } else {
            print("NOT EMPTY \(trackingItems.count)")
            mainTableView.backgroundView = nil
        }
        
        selectObject()
        mainTableView.reloadData()
        navigationItem.title = "Easy Tracking"
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trackingItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let items : TrackingItems
        items = trackingItems[indexPath.item]

        let cell = mainTableView.dequeueReusableCell(withIdentifier: "trackingViewCell", for: indexPath) as! TrackingTableViewCell

        var imageDefault = "null_reference"

        if items.status! == "Entregue"{
            imageDefault = "success"
        } else if items.status! == "Encaminhado"{
            imageDefault = "encaminhado"
        } else if items.status! == "Postado"{
            imageDefault = "postado"
        } else if items.status! == "Saiu para entrega ao destinatário"{
            imageDefault = "saiu"
        } else if items.status!  == "Entregue"{
            imageDefault = "success"
        } else if items.status!  == "Área com distribuição sujeito a prazo diferenciado" {
            imageDefault = "time"
        } else if items.status! == "Saida para entrega cancelada"{
            imageDefault = "n_entregue"
        } else if items.status! == "Favor desconsiderar a informação anterior"{
            imageDefault = "n_entregue"
        } else {
            imageDefault = "null_reference"
        }
        
        let status = items.status!

        cell.commonInit(identify: items.identify!, codString: items.cod!, statusString: status, imageName: imageDefault)
        
//        print("COD ------> \(items.cod)")
//        print("IDENTIFY ------> \(items.identify)")
        return cell
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let items : TrackingItems
        items = trackingItems[indexPath.row]
        
        let vcName = "second"
        let destination = self.storyboard?.instantiateViewController(withIdentifier: vcName) as! TrackViewController
        destination.stringPassed = items.cod!
        self.navigationController?.pushViewController(destination, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//
//        let items : TrackingItems
//        items = trackingItems[indexPath.row]
//
//        if(editingStyle == UITableViewCellEditingStyle.delete){
//            deleteObject(with: items.cod!)
//            print(items.cod!)
//        }
//    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let items : TrackingItems
        items = trackingItems[indexPath.row]
        
        let delete = UITableViewRowAction(style: .destructive, title: "Excluir"){
            (action, indexPath) in
            self.deleteObject(with: items.cod!)
            print(items.cod!)
        }
        
        return[delete]
    }
    
    func getJSONFromUrl(cod: String?){
        print("COD \(cod!)")
        

        
        let URL = "https://api.postmon.com.br/v1/rastreio/ect/" + cod!
        
        print(URL)
        
        
        let url = NSURL(string: URL)
        
        var i = 0
        
        var situacaoFinal = ""
        
        URLSession.shared.dataTask(with: (url as URL?)!, completionHandler: {(data, response, error) -> Void in
            
            if data == nil{
                print("Sem conexão \(String(describing: data))")
            } else{
            if let jsonObj = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary{

                print(jsonObj!.value(forKey: "historico")!)
                if let jsonArray = jsonObj!.value(forKey: "historico") as? NSArray{
                    
                    let count = jsonArray.count
                    print("COUNT \(count)")
                    
                    for array in jsonArray{
                        if let vetsDict = array as? NSDictionary {
                            _ = vetsDict.value(forKey: "situacao")
                            
                            let situacao = vetsDict.value (forKey: "situacao")
                    
                            print("ARRAY \(jsonArray.firstObject!)")
                            
                            i = i + 1
                            print(i)
                            
                            if(i == jsonArray.count){
                                
                                situacaoFinal = situacao as! String
            
                                var statusNew = " "
                                
                                if situacaoFinal == "Objeto Entregue"{
                                    statusNew = "Entregue"
                                    self.updateObject(with: cod, status: statusNew)
                                }
                                
                                if situacaoFinal == "Objeto encaminhado"{
                                    statusNew = "Encaminhado"
                                    self.updateObject(with: cod, status: statusNew)
                                }
                                
                                if situacaoFinal == "Objeto postado"{
                                    statusNew = "Postado"
                                    self.updateObject(with: cod, status: statusNew)
                                }
                                
                                if situacaoFinal == "Objeto saiu para entrega ao destinatï¿½rio"{
                                    statusNew = "Saiu para entrega ao destinatário"
                                    self.updateObject(with: cod, status: statusNew)
                                }
                                
                                if situacaoFinal  == "Objeto entregue ao destinatï¿½rio"{
                                    statusNew = "Entregue"
                                    self.updateObject(with: cod, status: statusNew)
                                }
                                
                                
                                if situacaoFinal  == "ï¿½rea com distribuiï¿½ï¿½o sujeita a prazo diferenciado" {
                                    statusNew = "Área com distribuição sujeito a prazo diferenciado"
                                    self.updateObject(with: cod, status: statusNew)
                                }
                                
                                if situacaoFinal == "Objeto aguardando retirada no endereï¿½o indicado"{
                                    statusNew = "Objeto aguardando retirada no endereço do indicado"
                                    self.updateObject(with: cod, status: statusNew)
                                }
                                
                                if situacaoFinal == "Favor desconsiderar a informaï¿½ï¿½o anterior"{
                                    statusNew = "Favor desconsiderar a informação anterior"
                                    self.updateObject(with: cod, status: statusNew)
                                }
                                
                                if situacaoFinal == "Saï¿½da para entrega cancelada"{
                                    statusNew = "Saida para entrega cancelada"
                                    self.updateObject(with: cod, status: statusNew)
                                }
                                
                                if situacaoFinal == "Objeto postado apï¿½s o horï¿½rio limite da unidade"{
                                    statusNew = "Objeto postado após o horário limite da unidade"
                                    self.updateObject(with: cod, status: statusNew)
                                }
                                else {
                                    self.updateObject(with: cod, status: situacaoFinal)
                                }
                            }
                        
                            OperationQueue.main.addOperation({
                                self.mainTableView.reloadData()
                            })
                        }
                    }
                    
                }
            }
        }
    }).resume()

}

    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

