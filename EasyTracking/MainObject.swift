//
//  MainObject.swift
//  EasyTracking
//
//  Created by Willian Calazans on 26/03/18.
//  Copyright © 2018 alzan. All rights reserved.
//

import Foundation
import CoreData

class Objects: NSManagedObject {
    @NSManaged var title:String
    @NSManaged var cod:String
    @NSManaged var status:String
    @NSManaged var category:String

}
