//
//  TrackingDB.swift
//  EasyTracking
//
//  Created by Willian Calazans on 26/03/18.
//  Copyright © 2018 alzan. All rights reserved.
//

import Foundation
import SQLite3

class TrackingDB{
    
    var db: OpaquePointer?
    
    func createDB(){
        
        let fileUrl = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("AlzanDB.sqlite")
        
        if sqlite3_open(fileUrl.path, &db) == SQLITE_OK{
            print("OK DB")
            createTable()
        }
    }
    
    var trackingItems = [TrackingItems]()

    
    func createTable(){
        
        var stmt: OpaquePointer?
        
        
        let createTable = "CREATE TABLE IF NOT EXISTS OBJETOS (ID INTEGER PRIMARY KEY, COD TEXT UNIQUE, ARCHIVE TEXT, STATUS TEXT, IDENTIFY TEXT)"
        
        if sqlite3_exec(db, createTable, nil, nil, nil) != SQLITE_OK{
            let errMsg = String(cString: sqlite3_errmsg(db))
            print("ERROR CREATE TABLE \(errMsg)")
        } else {
            print("That`s OK")
            
            let insertQuery = "INSERT INTO OBJETOS (COD, IDENTIFY) VALUES (?, ?)"
            
            
            if sqlite3_prepare(db, insertQuery, -1, &stmt, nil) != SQLITE_OK{
                let errMsg = String(cString: sqlite3_errmsg(db))
                print("ERROR PREPARE INSERT \(errMsg)")
                return
            } else {
                print("INSERT OK")
            }
            
            if sqlite3_bind_text(stmt, 1, "TESTE", -1, nil) != SQLITE_OK{
                let errMsg = String(cString: sqlite3_errmsg(db))
                print("ERROR BIND 1 INSERT \(errMsg)")
                return
            }
            else {
                print("INSERT OK")
            }
            
            if sqlite3_bind_text(stmt, 2, "TESTE", -1, nil) != SQLITE_OK{
                let errMsg = String(cString: sqlite3_errmsg(db))
                print("ERROR BIND 2 INSERT \(errMsg)")
                return
                
            } else {
                print("INSERT OK")
            }
            
            if sqlite3_step(stmt) != SQLITE_DONE{
                let errMsg = String(cString: sqlite3_errmsg(db))
                print("ERROR STEP INSERT \(errMsg)")
                return
            } else {
                print("INSERT OK")
            }
            
            let selectQuery = "SELECT COD, IDENTIFY FROM OBJETOS"
            
            if sqlite3_prepare(db, selectQuery, -1, &stmt, nil) != SQLITE_OK {
                let errMsg = String(cString: sqlite3_errmsg(db))
                print("SELECT PREPARE INSERT \(errMsg)")
            }
            
            while (sqlite3_step(stmt) == SQLITE_ROW){
                let COD = sqlite3_column_text(stmt, 1)
                let IDENTIFY = sqlite3_column_text(stmt, 2)
                
                trackingItems.append(TrackingItems(cod: String(describing: COD), identify: String(describing: IDENTIFY)))
                
                print("COUNT \(trackingItems.count)")
            }
            
        }
        
    }
    
    
    func insertObject(with cod : String, ident : String){
        var stmt: OpaquePointer?
        
        let insertQuery = "INSERT INTO OBJETOS (COD, IDENTIFY) VALUES (?, ?)"
        
        if sqlite3_prepare(db, insertQuery, -1, &stmt, nil) != SQLITE_OK{
            let errMsg = String(cString: sqlite3_errmsg(db))
            print("ERROR PREPARE INSERT \(errMsg)")
        }
        
        if sqlite3_bind_text(stmt, 1, cod, -1, nil) != SQLITE_OK{
            let errMsg = String(cString: sqlite3_errmsg(db))
            print("ERROR BIND 1 INSERT \(errMsg)")        }
        
        if sqlite3_bind_text(stmt, 2, ident, -1, nil) != SQLITE_OK{
            let errMsg = String(cString: sqlite3_errmsg(db))
            print("ERROR BIND 2 INSERT \(errMsg)")        }
        
        if sqlite3_step(stmt) != SQLITE_DONE{
            let errMsg = String(cString: sqlite3_errmsg(db))
            print("ERROR STEP INSERT \(errMsg)")        }
    }
    
    func selectObject(){
    }
}
