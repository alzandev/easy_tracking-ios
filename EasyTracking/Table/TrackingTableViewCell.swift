//
//  TrackingTableViewCell.swift
//  EasyTracking
//
//  Created by Willian Calazans on 06/12/18.
//  Copyright © 2018 alzan. All rights reserved.
//

import UIKit

class TrackingTableViewCell: UITableViewCell {
    @IBOutlet weak var trackView: UIView!
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var cod: UILabel!
    @IBOutlet weak var status: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        trackView.layer.cornerRadius = 7.0
        trackView.layer.shadowColor = UIColor.lightGray.cgColor
        trackView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        trackView.layer.shadowRadius = 6.0
        trackView.layer.shadowOpacity = 0.35
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func commonInit(identify: String, codString: String, statusString: String, imageName : String){
        mainImage.image = UIImage(named: imageName)
        title.text = identify
        cod.text = codString
        status.text = statusString
    }
    
}
