//
//  IdentifyItems.swift
//  EasyTracking
//
//  Created by Willian Calazans on 27/03/18.
//  Copyright © 2018 alzan. All rights reserved.
//

import Foundation

class IdentifyItems {
    var identify: String?
    
    init(identify: String?){
        self.identify = identify
    }
}
