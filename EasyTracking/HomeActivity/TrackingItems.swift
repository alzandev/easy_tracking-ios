//
//  TrackingItems.swift
//  EasyTracking
//
//  Created by Willian Calazans on 27/03/18.
//  Copyright © 2018 alzan. All rights reserved.
//

import Foundation

class TrackingItems {
    var cod: String?
    var identify: String?
    var status: String?
    
    init(cod: String?, identify: String?, status: String?){
        self.cod = cod
        self.identify = identify
        self.status = status
    }
}
