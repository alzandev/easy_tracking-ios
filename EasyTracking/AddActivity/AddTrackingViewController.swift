//
//  CustomAlertViewController.swift
//  EasyTracking
//
//  Created by Willian Calazans on 25/03/18.
//  Copyright © 2018 alzan. All rights reserved.
//

import UIKit

class AddTrackingViewController: UIViewController, UITextFieldDelegate {
    // MARK: - Navigation
    
    @IBOutlet weak var codLabel: UITextField!
    @IBOutlet weak var identLabel: UITextField!
    
    @IBOutlet weak var label: UILabel!
    @IBAction func action(_ sender: UIBarButtonItem) {
        
        let cod = codLabel.text
        let ident = identLabel.text
        
        ViewController().createDB()
        ViewController().insertObject(with : cod!, ident : ident!)
        ViewController().insertCod = cod!
        
        
       _ = navigationController?.popToRootViewController(animated: true)
        
                
    }
    
    func textField(_ textField:UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        let maxLength = 13
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string.uppercased()) as NSString
        return newString.length <= maxLength
    }
    
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
   /* override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    */

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        codLabel.autocapitalizationType = .allCharacters
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.title = "Adicionar Objeto"
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }


}
