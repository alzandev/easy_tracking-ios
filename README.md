**Easy Tracking | Rastreio de Encomendas**


Gerencie e acompanhe suas encomendas dos Correios em um só lugar.


Recursos: 

 - Acompanhe sua encomenda da postagem a entrega.
 - Tela especial para encomendas: (Entregues, Em Andamento e/ou Arquivadas).
 - Suporta todos os serviços de encomenda dos Correios, Nacionais e Internacionais (PAC, SEDEX, SEDEX 10,...).
 - Interface intuitiva e simples que facilita o rastreio.